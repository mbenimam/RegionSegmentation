from sklearn.metrics import precision_recall_curve
from IcyXml import *
from datasets import *
from models import *
from unet import UNet
from pycocotools.coco import COCO
from UNeXt.losses import BCEDiceLoss
from UNeXt.archs import UNext
import argparse
import numpy as np
import pycocotools
import pandas as pd
import matplotlib.pyplot as plt
import json
import cv2
import os
import sys
import tqdm
from scipy.spatial.distance import cdist
# import shapely
# from shapely.geometry import Polygon
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from torchvision import io
import albumentations as A
import albumentations.pytorch.transforms as T
import pytorch_lightning as pl
from torch import threshold
from torchvision.utils import make_grid
from scipy.optimize import linear_sum_assignment

# add src to path
sys.path.append('src')


numberOfThresholds = 100


def getRegionEvaluations(masks, masksPrediction, thresholds=10):
    # calculate true positives, false positives, false negatives for different thresholds for each region dice score
    # masks is a list of masks
    # masksPrediction is a list of masks
    # thresholds is a list of thresholds
    # the region dice score is the dice score between the mask and the maskPrediction
    # the region dice score is calculated for each mask and each maskPrediction
    # the region dice score is calculated for each threshold
    # the true positives, false positives, false negatives are calculated for each threshold

    masks = masks.cpu().numpy()

    regionEvaluations = np.zeros((2, 3, thresholds))
    # print(
    # f"masks shape = {masks.shape, masks.dtype}, prediction mask shape = {masksPrediction.shape, masksPrediction.dtype}")

    for i in range(len(masks)):
        for j, threshold in enumerate(np.linspace(0, 0.6, thresholds)):
            thresholdedPredictionMask = masksPrediction[i] > threshold
            # compute true positives, false positives, false negatives
            # true positives are the number of pixels in the mask that are also in the prediction mask
            # false positives are the number of pixels in the prediction mask that are not in the mask
            # false negatives are the number of pixels in the mask that are not in the prediction mask

            # true positives
            tp = np.sum(np.logical_and(masks[i], thresholdedPredictionMask))
            # false positives
            fp = np.sum(np.logical_and(np.logical_not(
                masks[i]), thresholdedPredictionMask))
            # false negatives
            fn = np.sum(np.logical_and(
                masks[i], np.logical_not(thresholdedPredictionMask)))
            regionEvaluations[i, 0, j] = tp
            regionEvaluations[i, 1, j] = fp
            regionEvaluations[i, 2, j] = fn

    return regionEvaluations


def getCellsEvalutions(points, pointsPrediction, thresholds=10, minDist=20):
    # build a square matrix of distances between points and pointsPrediction
    # each row is a point, each column is a pointPrediction
    # each element is the distance between the point and the pointPrediction
    # the distance is the euclidean distance between the points

    # points is a list of points
    # pointsPrediction is a list of points

    # dist = np.zeros((len(points), len(pointsPrediction)))
    # for i in range(len(points)):
    #     for j in range(len(pointsPrediction)):
    #         dist[i, j] = np.linalg.norm(points[i] - pointsPrediction[j])
    # use scipy.spatial.distance.cdist
    # print(f"points = {points}, pointsPrediction = {pointsPrediction}")
    if len(pointsPrediction) == 0:
        return 0, 0, len(points)
    if(len(points) == 0):
        return 0, len(pointsPrediction), 0

    dist = cdist(points, pointsPrediction)
    # dist = np.where(dist < minDist, np.inf, dist)
    # print(dist)

    # solve the assignment problem
    # the assignment problem is to find the minimum cost assignment
    # the cost is the distance between the points
    # the assignment problem is solved using the Hungarian algorithm
    # the Hungarian algorithm is a special case of the linear sum assignment problem
    # the Hungarian algorithm returns two arrays
    # row_ind is an array of row indices
    # col_ind is an array of column indices
    # the row_ind[i] is the row index of the i-th column index
    # the col_ind[i] is the column index of the i-th row index
    # the row_ind[i] and col_ind[i] are the indices of the points and pointsPrediction that are assigned to each other

    row_ind, col_ind = linear_sum_assignment(dist)
    # remove associations with a distance greater than minDist
    row_ind, col_ind = row_ind[dist[row_ind, col_ind] <
                               minDist], col_ind[dist[row_ind, col_ind] < minDist]

    # the points that are not assigned to any pointPrediction are false negatives
    # the pointsPrediction that are not assigned to any point are false positives
    # the points that are assigned to a pointPrediction are true positives

    truePositives = 0
    falsePositives = 0
    falseNegatives = 0

    # # for each point
    # for i in range(len(points)):
    #     # if the point is not assigned to any pointPrediction
    #     if(i not in row_ind):
    #         # the point is a false negative
    #         falseNegatives += 1
    #     else:
    #         # the point is a true positive
    #         truePositives += 1

    # # for each pointPrediction
    # for i in range(len(pointsPrediction)):
    #     # if the pointPrediction is not assigned to any point
    #     if(i not in col_ind):
    #         # the pointPrediction is a false positive
    #         falsePositives += 1
    truePositives = len(row_ind)
    falsePositives = len(pointsPrediction) - len(row_ind)
    falseNegatives = len(points) - len(row_ind)

    # compute the average distance between the points and the pointsPrediction
    # the average distance is the average of the distances between the points and the pointsPrediction
    distances = dist[row_ind, col_ind]
    averageDistance = np.mean(distances)
    print(f"average distance = {averageDistance}")

    return truePositives, falsePositives, falseNegatives


parser = argparse.ArgumentParser(
    description='Train a model', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# inference
parser.add_argument('--model', type=str, default='unext',
                    help='model to train')
parser.add_argument('--numClasses', type=int,
                    default=5, help='number of classes')
parser.add_argument('--gpu', type=int, default=1, help='gpu to be used')
parser.add_argument('--checkpointName', type=str,
                    default="version_21/checkpoints/epoch=292-val_loss=0.35.ckpt", help='weights to be used')

parser.add_argument('--rootDir', type=str,
                    default='/data', help='root directory')
parser.add_argument('--annFile', type=str,
                    default='annotations.json', help='annotation file')
parser.add_argument('--imgSize', type=int, default=2048, help='image size')
parser.add_argument('--device', type=str, default='cpu',
                    help='device to be used')
# add parser for thresholds for each class
parser.add_argument('--thresholds', type=float, nargs='+',
                    default=[0.5, 0.5, 0.5, 0.5, 0.5], help='thresholds for each class')


args = parser.parse_args()
rootDir = args.rootDir
checkpointName = args.checkpointName
device = args.device
imgSize = args.imgSize


transform = A.Compose([
    A.Crop(0, 0, imgSize, imgSize),
    T.ToTensorV2(transpose_mask=True),
])
dataset_test = LungTumorDatasetEvaluation(rootDir, args.annFile, transform)

if(args.model == "unext"):
    model = UNext(args.numClasses, 3, False)
elif(args.model == "unet"):
    model = UNet(3, args.numClasses, False)


plModel = SegModel(model, None)
plModel = plModel.load_from_checkpoint(checkpointName, backbone=model)
plModel.eval()

model = plModel.to(args.device)

nameFromColor = {"-256": "CD3", "-65536": "Nkp46", "-16711706": "Tryptase"}
colorFromName = {"CD3": "-256", "Nkp46": "-65536", "Tryptase": "-16711706"}
nameFromId = {0: "Nkp46", 1: "Tryptase", 2: "CD3"}
regionNameFromId = {0: "Tumor islet", 1: "Necrotic"}
# thresholds = [0.5, 0.5, 0.98, 0.98, 0.98]
thresholds = args.thresholds

# for each cell type of the 3 cell types make an array for true positives, false positives, false negatives for each threshold
# for each threshold, calculate precision and recall
# plot precision recall curve for each cell type
# plot average precision recall curve for all cell types

# for each cell type, for each threshold, calculate true positives, false positives, false negatives
cellsEvaluations = np.zeros((3, 3, numberOfThresholds))

# for each segmentation class
regionEvaluations = np.zeros((2, 3, numberOfThresholds))


with torch.no_grad():

    for k in tqdm.tqdm(range(len(dataset_test))):
        # for each image
        # torch.cuda.empty_cache()
        img, gtMask, points = dataset_test[k]
        print(f"img dimensions = {img.shape}")
        img_id = dataset_test.ids[k]

        out = model(img.unsqueeze(0).to(device))
        out = torch.sigmoid(out).squeeze(0).cpu().numpy()

        out = out.astype(np.float32)
        densityMaps = out[-3:]
        segmentations = out[:-3]


#       #
        print("Computing region evaluations")
        regionEvaluations += getRegionEvaluations(
            gtMask, segmentations, numberOfThresholds)

        print("Computing cells evaluations")

        print("Nkp46 cells detection ... ")

        for j, threshold in enumerate(np.linspace(0.5, 0.6, numberOfThresholds)):

            densityMap = densityMaps[-3]
            pointsNk = np.array(findIsolatedLocalMaxima(
                densityMap, threshold=threshold))
            tp, fp, fn = getCellsEvalutions(
                points[0], pointsNk, numberOfThresholds)
            # print(
            #     f"true positives = {tp}, false positives = {fp}, false negatives = {fn}")
            cellsEvaluations[0, 0, j] += tp
            cellsEvaluations[0, 1, j] += fp
            cellsEvaluations[0, 2, j] += fn

        print("Tryptase cells detection ... ")
        for j, threshold in enumerate(np.linspace(0.5, 0.7, numberOfThresholds)):

            densityMap = densityMaps[-2]
            pointsTryptase = np.array(findIsolatedLocalMaxima(
                densityMap, threshold=threshold))
            tp, fp, fn = getCellsEvalutions(
                points[1], pointsTryptase, numberOfThresholds)
            # print(
            #     f"true positives = {tp}, false positives = {fp}, false negatives = {fn}")
            cellsEvaluations[1, 0, j] += tp
            cellsEvaluations[1, 1, j] += fp
            cellsEvaluations[1, 2, j] += fn

        print("CD3 cells detection ... ")
        for j, threshold in enumerate(np.linspace(0.5, 1, numberOfThresholds)):

            densityMap = densityMaps[-1]
            pointsCD = findIsolatedLocalMaxima(densityMap, threshold=threshold)
            tp, fp, fn = getCellsEvalutions(
                points[2], pointsCD, numberOfThresholds)
            # print(
            #     f"true positives = {tp}, false positives = {fp}, false negatives = {fn}")
            cellsEvaluations[2, 0, j] += tp
            cellsEvaluations[2, 1, j] += fp
            cellsEvaluations[2, 2, j] += fn

    # once all the images are processed, calculate the precision and recall for each cell type and each threshold
    # for each cell type
    print(cellsEvaluations)
    plt.figure(figsize=(10, 10))
    for i in range(3):
        # store the precision and recall for each threshold
        precisionRecall = np.zeros((2, numberOfThresholds))

        # for each threshold
        for j in range(numberOfThresholds):
            # prevent division by 0
            # calculate the precision and recall
            if cellsEvaluations[i, 0, j] == 0:
                precision = 1
                recall = 0
            else:

                precision = cellsEvaluations[i, 0, j] / \
                    (cellsEvaluations[i, 0, j] + cellsEvaluations[i, 1, j])
                recall = cellsEvaluations[i, 0, j] / \
                    (cellsEvaluations[i, 0, j] + cellsEvaluations[i, 2, j])

            precisionRecall[0, j] = precision
            precisionRecall[1, j] = recall

        # plot the precision recall curve for the cell type
        plt.plot(precisionRecall[1, :], precisionRecall[0, :], 'o-',
                 label="Precision Recall Curve for " + nameFromId[i])
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.title("Precision Recall Curve for all cell types")
    plt.legend()
    plt.savefig("precisionRecallCurve_Cells.png")

    # for each region type
    print(regionEvaluations)
    plt.figure(figsize=(10, 10))
    for i in range(2):
        # store the precision and recall for each threshold
        precisionRecall = np.zeros((2, numberOfThresholds))

        # for each threshold
        for j in range(numberOfThresholds):
            # calculate the precision and recall
            if regionEvaluations[i, 0, j] == 0:
                precision = 1
                recall = 0
            else:
                precision = regionEvaluations[i, 0, j] / \
                    (regionEvaluations[i, 0, j] + regionEvaluations[i, 1, j])
                recall = regionEvaluations[i, 0, j] / \
                    (regionEvaluations[i, 0, j] + regionEvaluations[i, 2, j])
            precisionRecall[:, j] = [precision, recall]

        # reverse the order of the precision recall curve
        precisionRecall = precisionRecall[:, ::-1]
        # plot the precision recall curve for the cell type
        plt.plot(precisionRecall[1, :], precisionRecall[0, :], 'o-',
                 label="Precision Recall Curve for " + regionNameFromId[i])
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.title("Precision Recall Curve for all region types")
    plt.legend()
    plt.savefig("precisionRecallCurve_Regions.png")
