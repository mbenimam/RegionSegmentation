import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import xml.etree.ElementTree as ET
import os
import shutil
from tifffile import imsave


def findIsolatedLocalMaxima(greyScaleImage, threshold=1):
    squareDiameterLog3 = 2  # 27x27

    total = greyScaleImage
    for axis in range(2):
        d = 1
        for i in range(squareDiameterLog3):
            total = np.maximum(total, np.roll(total, d, axis))
            total = np.maximum(total, np.roll(total, -d, axis))
            d *= 2

    maxima = total == greyScaleImage
    h, w = greyScaleImage.shape

    result = []
    for j in range(h):
        for i in range(w):
            if maxima[j][i] and greyScaleImage[j, i] > threshold:
                result.append((i, j))
    return result


class IcyXml(object):
    def __init__(self, root_dir, ann_file, img_path, checkpointName, tif=False):
        """
        Args:
        """
        self.root_dir = root_dir
        self.ann_file = ann_file
        self.img_path = img_path
        self.checkpointName = checkpointName

        self.dataset_name = self.ann_file.split("/")[0]
        self.filename = self.ann_file.split("/")[1]
        self.img_filename = self.img_path.split("/")[1]

        xml = ET.parse(os.path.join(self.root_dir, self.ann_file))
        root = xml.getroot()
        self.name = root.find("name").text

        positionX = root.find("meta").find("positionX").text
        positionY = root.find("meta").find("positionY").text
        pixelSizeX = root.find("meta").find("pixelSizeX").text
        pixelSizeY = root.find("meta").find("pixelSizeY").text
        self.createMetadata(positionX, positionY, pixelSizeX, pixelSizeY, tif)

        self.rois = []
        self.lut = ""
        self.tif = tif
        if self.tif:
            self.createLut()
        # self.img_path = img_path

    def createMetadata(self, positionX, positionY, pixelSizeX, pixelSizeY, tif=False):
        """
        """
        if tif:
            channels = "<channelName0>Tumor Ilot</channelName0>\
                        <channelName1>Tumor</channelName1>\
                        <channelName2>Fibrotic</channelName2>\
                        <channelName3>Necrotic</channelName3>\
                        <channelName4>BloodVessel</channelName4>\
                        <channelName5>hole</channelName5>\
                        <channelName6>mucin</channelName6>\
                        <channelName7>white</channelName7>\
                        <channelName8>ash</channelName8>\
                        <channelName9>Nkp46</channelName9>\
                        <channelName10>Tryptase</channelName10>\
                        <channelName11>CD3</channelName11>"
        else:
            channels = "<channelName0>ch 0</channelName0>\
                        <channelName1>ch 1</channelName1>\
                        <channelName2>ch 2</channelName2>"

        self.meta = f"<positionX>{positionX}</positionX>\
            <positionY>{positionY}</positionY>\
            <positionZ>0</positionZ>\
            <positionT>0</positionT>\
            <pixelSizeX>{pixelSizeX}</pixelSizeX>\
            <pixelSizeY>{pixelSizeY}</pixelSizeY>\
            <pixelSizeZ>1</pixelSizeZ>\
            <timeInterval>1</timeInterval>\
            {channels}"

    def createLut(self):
        """
        """
        self.lut = "<numChannel>12</numChannel> <scaler0> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>1.0E-17</absleftin> <absrightin>1</absrightin> <leftin>6.829974069568696E-17</leftin> <rightin>0.9999910593032837</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler0> <colormap0> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap0> <scaler1> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0.1</absleftin> <absrightin>1</absrightin> <leftin>0.5907140374183655</leftin> <rightin>1</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler1> <colormap1> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap1> <scaler2> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0</absleftin> <absrightin>1</absrightin> <leftin>0</leftin> <rightin>0.20899762213230133</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler2> <colormap2> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap2> <scaler3> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>1.0E-33</absleftin> <absrightin>1</absrightin> <leftin>6.679750321967495E-33</leftin> <rightin>0.38310664892196655</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler3> <colormap3> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>170</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap3> <scaler4> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0</absleftin> <absrightin>0.01</absrightin> <leftin>0</leftin> <rightin>0.005279102362692356</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler4> <colormap4> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>170</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap4> <scaler5> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0</absleftin> <absrightin>0.01</absrightin> <leftin>0</leftin> <rightin>0.005820947699248791</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler5> <colormap5> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>170</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap5> <scaler6> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>1.0E-29</absleftin> <absrightin>0.1</absrightin> <leftin>3.4697547859549365E-29</leftin> <rightin>0.04002562537789345</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler6> <colormap6> <name>Custom</name> <enabled>true</enabled> <type>GRAY</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap6> <scaler7> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0</absleftin> <absrightin>0.01</absrightin> <leftin>0</leftin> <rightin>0.007225922774523497</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler7> <colormap7> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap7> <scaler8> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0</absleftin> <absrightin>0.01</absrightin> <leftin>0</leftin> <rightin>0.004866646137088537</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler8> <colormap8> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap8> <scaler9> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0.1</absleftin> <absrightin>1</absrightin> <leftin>0.38679271936416626</leftin> <rightin>0.967879056930542</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler9> <colormap9> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>85</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap9> <scaler10> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0.1</absleftin> <absrightin>1</absrightin> <leftin>0.4687964618206024</leftin> <rightin>1</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler10> <colormap10> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>170</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap10> <scaler11> <cancross>false</cancross> <integerdata>false</integerdata> <absleftin>0.1</absleftin> <absrightin>1</absrightin> <leftin>0.37941059470176697</leftin> <rightin>1</rightin> <leftout>0</leftout> <rightout>255</rightout> </scaler11> <colormap11> <name>Custom</name> <enabled>true</enabled> <type>RGB</type> <red rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </red> <green rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>0</value> </point> </green> <blue rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>255</value> </point> </blue> <gray rawdata=\"false\"> <point> <index>0</index> <value>0</value> </point> <point> <index>255</index> <value>170</value> </point> </gray> <alpha rawdata=\"false\"> <point> <index>0</index> <value>255</value> </point> <point> <index>255</index> <value>255</value> </point> </alpha> </colormap11>"

    def addRoiPoint(self, x, y, name, color):
        """
        """
        point = "<roi>\
            <classname>plugins.kernel.roi.roi2d.ROI2DPoint</classname>\
            <name>{}</name>\
            <selected>false</selected>\
            <readOnly>false</readOnly>\
            <color>{}</color>\
            <stroke>2</stroke>\
            <opacity>0.3</opacity>\
            <showName>false</showName>\
            <z>-1</z>\
            <t>-1</t>\
            <c>-1</c>\
            <position>\
                <pos_x>{}</pos_x>\
                <pos_y>{}</pos_y>\
            </position>\
        </roi>".format(name, color[0] << 16 | color[1] << 8 | color[2] << 0, x, y)

        self.rois.append(point)

    def addPolygon(self, points, name, color):
        """
        """

        polygonPoints = [
            f"<point><pos_x>{point[0]}</pos_x><pos_y>{point[1]}</pos_y></point>" for point in points]

        polygon = "<roi>\
            <classname>plugins.kernel.roi.roi2d.ROI2DPolygon</classname>\
            <name>{}</name>\
            <selected>false</selected>\
            <readOnly>false</readOnly>\
            <color>{}</color>\
            <stroke>2</stroke>\
            <opacity>0.3</opacity>\
            <showName>false</showName>\
            <z>-1</z>\
            <t>-1</t>\
            <c>-1</c>\
            <position>\
                <pos_x>0</pos_x>\
                <pos_y>0</pos_y>\
            </position>\
            <points>\
                {}\
            </points>\
            </roi>".format(name, color[0] << 16 | color[1] << 8 | color[2] << 0, '\n'.join(polygonPoints))

        self.rois.append(polygon)

    def addColormaps():
        return

    def save(self, masks=None):
        """
        """
        xml = f"<root>\
                    <name>{self.name}</name>\
                    <meta>{self.meta}</meta>\
                    <rois>{self.rois}</rois>\
 <lut>{self.lut}</lut>             </root >"

        destination = os.path.join(
            self.root_dir, "results", self.dataset_name+"_"+self.checkpointName.replace("/", "."))
        os.makedirs(destination, exist_ok=True)

        if not self.tif:
            shutil.copy(os.path.join(self.root_dir, self.img_path),
                        os.path.join(destination, self.img_filename))

        if masks is not None:
            imsave(os.path.join(destination,
                   ".".join(self.filename.split(".")[:-1])+"_.tif"), masks[:, :, :], imagej=True, metadata={'axes': 'CYX'})
            tif = IcyXml(self.root_dir, self.ann_file,
                         self.img_path, self.checkpointName, tif=True)
            tif.filename = ".".join(self.filename.split(".")[:-1])+"_.xml"
            tif.save()
            # data.save()
        # print(os.path.join(destination, img_filename))
        with open(os.path.join(destination, self.filename), "w") as f:
            f.write(xml)
