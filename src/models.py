import numpy as np
import matplotlib.pyplot as plt
import cv2

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import io
import pytorch_lightning as pl

from UNeXt.archs import UNext
from UNeXt.losses import BCEDiceLoss
from torch.nn import MSELoss
from pycocotools.coco import COCO

from torchvision.ops.focal_loss import sigmoid_focal_loss
from AutomaticWeightedLoss import AutomaticWeightedLoss

kernel = cv2.getStructuringElement(
    cv2.MORPH_ELLIPSE, (43, 43))

kernel2 = cv2.getStructuringElement(
    cv2.MORPH_ELLIPSE, (11, 11))


class SegModel(pl.LightningModule):
    def __init__(self, backbone, transforms=None):
        super(SegModel, self).__init__()

        self.net = backbone
        self.transforms = transforms
        self.learning_rate = 1e-3
        self.loss1 = sigmoid_focal_loss  # BCEDiceLoss()
        self.loss2 = MSELoss(reduce=False)
        # self.loss2 = sigmoid_focal_loss
        # self.loss2 = torch.nn.BCEWithLogitsLoss()
        self.name = 'UNext'

        self.loss = AutomaticWeightedLoss(2)
        self.regressionWeights = torch.from_numpy(
            np.array([1.0, 0.5, 0.1])).reshape(1, 3, 1, 1)

    def forward(self, x):
        return self.net(x)

    def training_step(self, batch, batch_nb):
        img, mask = batch
        out = self.forward(img)
        # loss1 = self.loss1(out[:, :-3].contiguous(), mask[:, :-3].contiguous())
        loss1 = self.loss1(out[:, :-3].contiguous(), mask[:, :-3].contiguous(), reduction="mean")
        loss2 = self.loss2(out[:, -3:].contiguous(), mask[:, -3:].contiguous())
        loss2 = (loss2*self.regressionWeights.to(img.device)).mean()

        self.log("Segmentation_loss", loss1)
        self.log("Regression_loss", loss2)
        # print(loss1.shape, loss2.shape)
        # loss = loss1 + loss2
        loss = loss1 + loss2 
        # loss = self.loss(loss1, loss2)
        self.log('train_loss', loss)
        return {'loss': loss}

    def validation_step(self, batch, batch_nb):
        img, mask = batch
        # img = img.float()
        # mask = mask.float()
        out = self.forward(img)

        # loss1 = self.loss1(out[:, :-3], mask[:, :-3])
        # loss2 = self.loss2(out[:, -3:], mask[:, -3:])

        # loss1 = self.loss1(out[:, :-3].contiguous(), mask[:, :-3].contiguous())
        loss1 = self.loss1(out[:, :-3].contiguous(), mask[:, :-3].contiguous(), reduction="mean")

        loss2 = self.loss2(out[:, -3:].contiguous(), mask[:, -3:].contiguous())
        # focal loss specefic
        # loss2 = (loss2[:, 0]*self.regressionWeights[0] + loss2[:, 1] *
        #          self.regressionWeights[1] + loss2[:, 2]*self.regressionWeights[2]).sum()
        loss2 = (loss2*self.regressionWeights.to(img.device)).mean()

        self.logger.experiment.add_histogram("Nk", out[:, -3])
        self.logger.experiment.add_histogram("tryptase", out[:, -2])
        self.logger.experiment.add_histogram("cd3", out[:, -1])

        self.log("Val_Segmentation_loss", loss1)
        self.log("Val_Regression_loss", loss2)

        self.log("sigma1", self.loss.params[0])
        self.log("sigma2", self.loss.params[1])

        loss = loss1 + loss2   
        # loss = self.loss(loss1, loss2)

        self.log('val_loss', loss)

        # if batch_nb == 0:
        #     out = torch.sigmoid(out)[0].cpu().numpy()

        #         thresholds = [0.5, 0.5, 0.6, 0.6, 0.7]
        #         thresholdedMasks = np.zeros_like(out)
        #         for j in range(len(thresholds)):
        #         thresholdedMasks[j] = out[j] > thresholds[j]

        #         fig, axes = plt.subplots(1, 5, figsize=(10, 2), dpi=200)
        #         axes = axes.flatten()

            # create new masks thresholded

            # for j, mask in enumerate(out):
                # print(mask.shape)
                # print(f"{dataset_test.id2cat[j]}: {mask.min()}, {mask.max()}")

                # mask = ((mask > thresholds[j])*255).astype(np.uint8)

#                 if(j < 2):
#                     # mask = cv2.morphologyEx(mask, cv2.MORPH_ERODE, kernel)
#                     mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel2)
#                     mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

#                     # gaussian smooth mask
#                     mask = cv2.GaussianBlur(mask, (101, 101), 0)
#                     # threshold 0.5
#                     mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)[1]
#                     # mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

                # axes[j].imshow(mask)
                # axes[j].axis('off')
                # axes[j].set_title(f"{dataset_test.id2cat[j]}")

            # self.logger.experiment.add_figure("masks",
            #                                   fig, global_step=self.global_step)

            # plt.close(fig)

        return {'val_loss': loss}

    def configure_optimizers(self):
        opt = torch.optim.Adam([{"params": self.net.parameters()}, {"params": self.loss.parameters()}],
                                lr=self.learning_rate)
        sch = torch.optim.lr_scheduler.CosineAnnealingLR(opt, T_max=400)
        return [opt], [sch]
