import os
import sys
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
sys.path.append('src')



from unet import UNet
from IcyXml import *
from datasets import *
from models import *
from pycocotools.coco import COCO
from UNeXt.losses import BCEDiceLoss
from UNeXt.archs import UNext
import numpy as np
# import pycocotools
import pandas as pd
import matplotlib.pyplot as plt
# import json
import cv2
# import os

import tqdm
# import shapely
# from shapely.geometry import Polygon

import torch
# import torch.nn as nn
# import torch.nn.functional as F
# import torch.optim as optim
# import torchvision.transforms as transforms
# import torchvision.models as models
from torch.utils.data import Dataset, DataLoader

# from torchvision import io

import albumentations as A
import albumentations.pytorch.transforms as T

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
# from pytorch_lightning.loggers import TensorBoardLogger
# from pytorch_lightning.loggers import TestTubeLogger
# from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.callbacks import LearningRateMonitor



from typing import List
from typing import Optional

import optuna
from optuna.integration import PyTorchLightningPruningCallback
import pytorch_lightning as pl
import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split


import argparse
# create argparser
parser = argparse.ArgumentParser(description='Train a model', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# training
parser.add_argument('--model', type=str, default='unet', help='model to train')
parser.add_argument('--batchSize', type=int, default=1, help='batch size')
parser.add_argument('--epochs', type=int, default=100, help='number of epochs')
parser.add_argument('--lr', type=float, default=0.001, help='learning rate')
parser.add_argument('--logDir', type=str, default='logs', help='log directory')
parser.add_argument('--ckpt_save_top_k', type=int, default=1, help='checkpoint save top k')
parser.add_argument('--gpu', type=int, default=1, help='gpu to be used')
parser.add_argument('--seed', type=int, default=42, help='random seed')

# dataset 
parser.add_argument('--resume_from_checkpoint', type=str, default=None, help='resume from checkpoint')
parser.add_argument('--rootDir', type=str, default='/data', help='root directory where is the json file')
# parser.add_argument('--dataset', type=str, default='icy', help='dataset to train on')
parser.add_argument('--annFile', type=str, required=True, help='annotation json file')
parser.add_argument('--task', type=str, default='segmentation', help='task to train')
parser.add_argument('--numWorkers', type=int, default=0, help='number of workers')
parser.add_argument('--prefetchFactor', type=int, default=2, help='prefetch factor')

# Loss hyperparameters
parser.add_argument('--dp', type=bool, default=False, help='deep supervision')
parser.add_argument('--hl', type=bool, default=False, help='task weighting')
parser.add_argument('--loss', type=str, default='bce', help='loss function')

# Augmentation parameters
parser.add_argument('--hflip', type=float, default=0.5, help='horizontal flip probability')
parser.add_argument('--vflip', type=float, default=0.5, help='vertical flip probability')
parser.add_argument('--rotate', type=float, default=0.5, help='rotate probability')
parser.add_argument('--scale', type=float, default=0.5, help='scale probability')
parser.add_argument('--shear', type=float, default=0.5, help='shear probability')
parser.add_argument('--blur', type=float, default=0.5, help='blur probability')
parser.add_argument('--brightness', type=float, default=0.5, help='brightness probability')
parser.add_argument('--contrast', type=float, default=0.5, help='contrast probability')
parser.add_argument('--hue', type=float, default=0.5, help='hue probability')
parser.add_argument('--saturation', type=float, default=0.5, help='saturation probability')
parser.add_argument('--gamma', type=float, default=0.5, help='gamma probability')
parser.add_argument('--noise', type=float, default=0.5, help='noise probability')
parser.add_argument('--imgSize', type=int, default=256, help='image size')



if __name__ == '__main__':


    # parse args
    args = parser.parse_args()
    pl.seed_everything(args.seed)

    ########################################## Dataset ##########################################
    transform = A.Compose([
        A.HorizontalFlip(p=args.hflip),
        A.VerticalFlip(p=args.vflip),
        A.RandomRotate90(p=args.rotate),
        A.ColorJitter(brightness=args.brightness, contrast=args.contrast, saturation=args.saturation, hue=args.hue),
        A.PadIfNeeded(min_height=args.imgSize, min_width=args.imgSize,
                      border_mode=cv2.BORDER_CONSTANT, value=1, p=1.0),
        A.RandomCrop(height=args.imgSize, width=args.imgSize, p=1.0),
        T.ToTensorV2(transpose_mask=True),
    ])
    
    # Dataset creation
    dataset = LungTumorDataset(args.rootDir, args.annFile, transform)

    # split the dataset into train and validation 
    trainDataset, validDataset = torch.utils.data.random_split(
        dataset, [int(len(dataset) * 0.8), int(len(dataset) * 0.2)+1])

    # Dataloader for training
    trainDataLoader = DataLoader(trainDataset,
                                 batch_size=args.batchSize,
                                 shuffle=True,
                                 num_workers=args.numWorkers,
                                 pin_memory=True,
                                 prefetch_factor=args.prefetchFactor)

    # Dataloader for validation
    validDataLoader = DataLoader(validDataset,
                                 batch_size=args.batchSize,
                                 shuffle=True,
                                 num_workers=args.numWorkers,
                                 pin_memory=True,
                                 prefetch_factor=args.prefetchFactor)
    ########################################## Dataset ######################################


    ######################################### Model #########################################
    model = UNet(3, len(dataset.cats), False)
    plModel = SegModel(model, None)

    ######################################## Training #######################################
    callbacks = [
        ModelCheckpoint(
            monitor='val_loss',
            save_top_k=5,
            filename='{epoch:02d}-{val_loss:.2f}'
        ),
        LearningRateMonitor(logging_interval='step')
    ]


    trainer = pl.Trainer(max_epochs=args.epochs, gpus=[
                         args.gpu], precision=16, callbacks=callbacks, num_sanity_val_steps=1)
    trainer.fit(plModel, trainDataLoader, validDataLoader)


def objective(trial: optuna.trial.Trial) -> float:

    # We optimize the number of layers, hidden units in each layer and dropouts.
    n_layers = trial.suggest_int("n_layers", 1, 3)
    dropout = trial.suggest_float("dropout", 0.2, 0.5)
    output_dims = [
        trial.suggest_int("n_units_l{}".format(i), 4, 128, log=True) for i in range(n_layers)
    ]

    model = LightningNet(dropout, output_dims)
    datamodule = FashionMNISTDataModule(data_dir=DIR, batch_size=BATCHSIZE)

    trainer = pl.Trainer(
        logger=True,
        limit_val_batches=PERCENT_VALID_EXAMPLES,
        enable_checkpointing=False,
        max_epochs=EPOCHS,
        gpus=1 if torch.cuda.is_available() else None,
        callbacks=[PyTorchLightningPruningCallback(trial, monitor="val_acc")],
    )
    hyperparameters = dict(n_layers=n_layers, dropout=dropout, output_dims=output_dims)
    trainer.logger.log_hyperparams(hyperparameters)
    trainer.fit(model, datamodule=datamodule)

    return trainer.callback_metrics["val_acc"].item()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PyTorch Lightning example.")
    parser.add_argument(
        "--pruning",
        "-p",
        action="store_true",
        help="Activate the pruning feature. `MedianPruner` stops unpromising "
        "trials at the early stages of training.",
    )
    args = parser.parse_args()

    pruner: optuna.pruners.BasePruner = (
        optuna.pruners.MedianPruner() if args.pruning else optuna.pruners.NopPruner()
    )

    study = optuna.create_study(direction="maximize", pruner=pruner)
    study.optimize(objective, n_trials=100, timeout=600)

    print("Number of finished trials: {}".format(len(study.trials)))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: {}".format(trial.value))

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))
