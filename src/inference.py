import argparse
import numpy as np
import pycocotools
import pandas as pd
import matplotlib.pyplot as plt
import json
import cv2
import os
import sys
import tqdm
# import shapely
# from shapely.geometry import Polygon
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from torchvision import io
import albumentations as A
import albumentations.pytorch.transforms as T
import pytorch_lightning as pl
from torch import threshold
from torchvision.utils import make_grid
from scipy.optimize import linear_sum_assignment

# add src to path
sys.path.append('src')
from UNeXt.archs import UNext
from UNeXt.losses import BCEDiceLoss
from pycocotools.coco import COCO
from unet import UNet
from models import *
from datasets import *
from IcyXml import *
from sklearn.metrics import precision_recall_curve




parser = argparse.ArgumentParser(description='Train a model', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# inference
parser.add_argument('--model', type=str, default='unext', help='model to train')
parser.add_argument('--numClasses', type=int, default=5, help='number of classes')
parser.add_argument('--gpu', type=int, default=1, help='gpu to be used')
parser.add_argument('--checkpointName', type=str, default="version_21/checkpoints/epoch=292-val_loss=0.35.ckpt", help='weights to be used')

parser.add_argument('--rootDir', type=str, default='/data', help='root directory')
parser.add_argument('--annFile', type=str, default='annotations.json', help='annotation file')
parser.add_argument('--imgSize', type=int, default=2048, help='image size')
parser.add_argument('--device', type=str, default='cpu', help='device to be used')
parser.add_argument('--thresholds', type=float, nargs='+', default=[0.4, 0.3, 0.8, 0.9, 0.95], help='thresholds for each class')

args = parser.parse_args()
args = parser.parse_args()
rootDir = args.rootDir
checkpointName = args.checkpointName
device = args.device
imgSize = args.imgSize
# ann_files = [os.path.join(root_dir, f) for f in os.listdir(root_dir) if f.endswith('.json')]
transform = A.Compose([
    A.Crop(0, 0, imgSize, imgSize),
    T.ToTensorV2(transpose_mask=True),
])
dataset_test = LungTumorDataset(rootDir, args.annFile, transform)

dataset_test = LungTumorDatasetEvaluation(rootDir, args.annFile, transform)

if(args.model == "unext"):
    model = UNext(args.numClasses, 3, False)
elif(args.model == "unet"):
    model = UNet(3, args.numClasses, False)

# model = UNet(3, 12, False)
# checkpointName = "version_21/checkpoints/epoch=292-val_loss=0.35.ckpt"

plModel = SegModel(model, None)
plModel = plModel.load_from_checkpoint(checkpointName, backbone=model)
plModel.eval()

model = plModel.to(args.device)

nameFromColor = {"-256": "CD3", "-65536": "Nkp46", "-16711706": "Tryptase"}
colorFromName = {"CD3": "-256", "Nkp46": "-65536", "Tryptase": "-16711706"}

thresholds = args.thresholds

        ###################################### ICYYY STUFF ##################################################
with torch.no_grad():

    for i in tqdm.tqdm(range(len(dataset_test))):
        # torch.cuda.empty_cache()
        img, gtMask, _ = dataset_test[i]
        img_id = dataset_test.ids[i]

        out = model(img.unsqueeze(0).to(device))
        out = out.squeeze(0)
        # out = torch.sigmoid(out).cpu().numpy()
        outT = out
        # outT[:-3] = torch.sigmoid(outT[:-3])
        outT = torch.sigmoid(outT)
        densityMaps = torch.sigmoid(out[-3:]).cpu().numpy().astype(np.float32)
        outT = outT.cpu().numpy()
        out = out.cpu().numpy()
        out = out.astype(np.float32)
        
        # print(img_id)
        # img_path = os.path.join(dataset.root_dir, dataset.coco.imgs[img_id]['file_name'])
        img_path = dataset_test.coco.imgs[img_id]['file_name']
        annot_file = ".".join(img_path.split('.')[:-1]) + ".xml"
        # print(img_path, annot_file)

        
        thresholdedMasks = np.zeros_like(out)
        for j in range(len(thresholds)-3):
            thresholdedMasks[j] = outT[j] > thresholds[j]

        icyFile = IcyXml(rootDir, annot_file, img_path, checkpointName)

        contours = extract_contours(dataset_test, (thresholdedMasks[:-3]*255).astype(np.uint8))
        if(contours == []):
            print("No contours found")
        for j, (contour, name, color) in enumerate(contours):
            # print(name)

            icyFile.addPolygon(contour, name, color)

        # print("Tryptase cells detection ... ")
        densityMap = densityMaps[-2]
        # print(densityMap.min(), densityMap.max())
        points = np.array(findIsolatedLocalMaxima(densityMap, threshold=thresholds[3]))

        for j, point in enumerate(points):
            icyFile.addRoiPoint(point[0], point[1], "Tryptase", dataset_test.name2color["Tryptase"])


        # print("Nkp46 cells detection ... ")
        densityMap = densityMaps[-3]
        # print(densityMap.min(), densityMap.max())
        points = np.array(findIsolatedLocalMaxima(densityMap, threshold=thresholds[2]))

        for j, point in enumerate(points):
            icyFile.addRoiPoint(point[0], point[1], "Nkp46", dataset_test.name2color["Nkp46"])


        
        # print("CD3 cells detection ... ")
        densityMap = densityMaps[-1]
        # print(densityMap.min(), densityMap.max())
        points = findIsolatedLocalMaxima(densityMap, threshold=thresholds[4])

        for j, point in enumerate(points):
            icyFile.addRoiPoint(point[0], point[1], "CD3",  dataset_test.name2color["CD3"])

        icyFile.save(outT)

