import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# from matplotlib.patches import Polygon
import json
import os
import sys
import scipy.ndimage as ndimage
from albumentations.augmentations.crops.transforms import RandomCrop

import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader

from torchvision import io


from UNeXt.archs import UNext
from UNeXt.losses import BCEDiceLoss
from pycocotools.coco import COCO


class LungTumorDataset(Dataset):
    def __init__(self, root_dir, ann_file, transforms=None, imageSize=512):
        """
        Args:

            root_dir (string): Directory with all the images.
            ann_file (string): Path to the json file with annotations.
            transforms (callable, optional): Optional transform to be applied
                on a sample.

        """
        self.root_dir = root_dir
        self.transforms = transforms
        self.coco = COCO(ann_file)
        self.ids = list(self.coco.imgs.keys())
        self.cats = self.coco.loadCats(self.coco.getCatIds())
        self.cat2id = {cat['name']: cat['id'] for cat in self.cats}
        # self.color2id = {v: k for k, v in self.id2color.items()}
        self.id2cat = {cat['id']: cat['name'] for cat in self.cats}
        self.name2color = {'CD3': np.array([255, 255,   0]),
                           'Nkp46': np.array([255,   0,   0]),
                           'Tryptase': np.array([0, 255, 230]),
                           'Necrotic': np.array([255,   0, 255]),
                           'Fibrotic': np.array([218, 215, 215]),
                           'Blood vessel': np.array([0,   0, 255]),
                           'white': np.array([255, 253, 224]),
                           'hole': np.array([20, 20, 20]),
                           'Tumor Islet': np.array([255, 128,   0]),
                           'Tumor': np.array([255, 255, 184]),
                           'Ash': np.array([100, 100, 100]),
                           'mucin': np.array([214, 237, 255])}
        self.imageSize = imageSize

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, idx):
        """
        Args:
            idx (int): Index
            Returns:
            sample (dict): Sample
        """
        img_id = self.ids[idx]
        annotations = self.coco.loadAnns(self.coco.getAnnIds(imgIds=img_id))
        img_path = os.path.join(
            self.root_dir, self.coco.imgs[img_id]['file_name'])
        img = io.read_image(img_path, io.ImageReadMode.RGB)/255
        imgShape = img.shape

        masks = np.zeros(
            (len(self.cats), img.shape[1], img.shape[2]), dtype=np.float32)

        keypoints = {'CD3': [],
                     'Nkp46': [],
                     'Tryptase': []}

        for annotation in annotations:
            catId = annotation['category_id']
            # print(groundTruth[catId].shape)
            if("keypoint" in annotation):
                # add the keypoint to the dict
                if self.id2cat[catId] in keypoints:
                    keypoints[self.id2cat[catId]].append(
                        annotation['keypoint'])

            elif(len(annotation["segmentation"][0]) > 4):
                mask = self.coco.annToMask(annotation)
                masks[catId] = np.logical_or(
                    masks[catId], mask)

        CD3 = np.floor(np.array(keypoints["CD3"])).astype(int)
        Nkp46 = np.floor(np.array(keypoints["Nkp46"])).astype(int)
        Tryptase = np.floor(np.array(keypoints["Tryptase"])).astype(int)

        # masks = torch.from_numpy(masks).float()
        # add padding to the image and masks to make them the same size

        # add padding to the image and masks to make them the same size
        # padx = self.imageSize - imgShape[2]
        # pady = self.imageSize - imgShape[1]

        # if padx > 0 or pady > 0:
        #     finalImage = torch.ones((3, self.imageSize, self.imageSize)).float()
        #     finalMasks = torch.zeros((len(self.cats), self.imageSize, self.imageSize)).float()

        #     finalImage[:img.shape[0], :img.shape[1], :img.shape[2]] = img
        #     finalMasks[:, :img.shape[1], :img.shape[2]] = masks

        #     img = finalImage
        #     masks = finalMasks

        dots_mask = np.zeros((img.shape[1], img.shape[2], 3))

        if len(CD3) > 0:
            # CD3[:, 0] = np.clip(CD3[:, 0], 0, imgShape[1]-2)
            # CD3[:, 1] = np.clip(CD3[:, 1], 0, imgShape[0]-2)
            # print("CD3: ", CD3.min(), CD3.max())
            # print(dots_mask.shape, CD3.min(), CD3.max())
            dots_mask[CD3[:, 1], CD3[:, 0], 0] = 255.0

        if len(Nkp46) > 0:
            # Nkp46[:, 0] = np.clip(Nkp46[:, 0], 0, imgShape[1]-2)
            # Nkp46[:, 1] = np.clip(Nkp46[:, 1], 0, imgShape[0]-2)
            # print("Nkp46: ", Nkp46.min(), Nkp46.max())
            dots_mask[Nkp46[:, 1], Nkp46[:, 0], 1] = 255.0

        if len(Tryptase) > 0:
            # Tryptase[:, 0] = np.clip(Tryptase[:, 0], 0, imgShape[1]-2)
            # Tryptase[:, 1] = np.clip(Tryptase[:, 1], 0, imgShape[0]-2)
            # print("Tryptase: ", Tryptase.min(), Tryptase.max())
            dots_mask[Tryptase[:, 1], Tryptase[:, 0], 2] = 255.0

        dots_mask = cv2.GaussianBlur(dots_mask, (11, 11), 0)
#         dots_mask = ndimage.gaussian_filter(dots_mask, sigma=(21, 21), order=0) * 100

        # cv2.normalize(dots_mask, dots_mask, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F)
        # dots_mask = (dots_mask - dots_mask.min()) / \
        #     (dots_mask.max() - dots_mask.min())

        # dots_mask = torch.from_numpy(dots_mask).float()

        # dots_mask

        masks[self.cat2id["CD3"]] = dots_mask[:, :, 0]
        masks[self.cat2id["Nkp46"]] = dots_mask[:, :, 1]
        masks[self.cat2id["Tryptase"]] = dots_mask[:, :, 2]

        # img = torch.from_numpy(img).float()
        # masks = torch.from_numpy(masks).float()

        if self.transforms is not None:
            # print("TRANSOFRMING")
            augmented = self.transforms(image=img.numpy().transpose(
                1, 2, 0), mask=masks.transpose(1, 2, 0))
            img, masks = augmented["image"], augmented["mask"]

        return img, masks

    def get_cat_id(self, cat_name):
        return self.cat2id[cat_name]

    def get_cat_name(self, cat_id):
        return self.id2cat[cat_id]

    def exportXml(self, img_id, mask):
        """
        export the mask to icy xml format
        Args:
            img_id (int): Index
            mask (np.array): Mask
            output_dir (string): Directory to save the xml file.
        """
        img_path = os.path.join(
            self.root_dir, self.coco.imgs[img_id]['file_name'])

        xml_filename = os.path.split(img_path)[1].split('.')[0] + '.xml'


class LungTumorDatasetEvaluation(Dataset):
    def __init__(self, root_dir, ann_file, transforms=None, imageSize=512):
        """
        Args:

            root_dir (string): Directory with all the images.
            ann_file (string): Path to the json file with annotations.
            transforms (callable, optional): Optional transform to be applied
                on a sample.

        """
        self.root_dir = root_dir
        self.transforms = transforms
        self.coco = COCO(ann_file)
        self.ids = list(self.coco.imgs.keys())
        self.cats = self.coco.loadCats(self.coco.getCatIds())
        self.cat2id = {cat['name']: cat['id'] for cat in self.cats}
        # self.color2id = {v: k for k, v in self.id2color.items()}
        self.id2cat = {cat['id']: cat['name'] for cat in self.cats}
        self.name2color = {'CD3': np.array([255, 255,   0]),
                           'Nkp46': np.array([255,   0,   0]),
                           'Tryptase': np.array([0, 255, 230]),
                           'Necrotic': np.array([255,   0, 255]),
                           'Fibrotic': np.array([218, 215, 215]),
                           'Blood vessel': np.array([0,   0, 255]),
                           'white': np.array([255, 253, 224]),
                           'hole': np.array([20, 20, 20]),
                           'Tumor Islet': np.array([255, 128,   0]),
                           'Tumor': np.array([255, 255, 184]),
                           'Ash': np.array([100, 100, 100]),
                           'mucin': np.array([214, 237, 255])}
        self.imageSize = imageSize

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, idx):
        """
        Args:
            idx (int): Index
            Returns:
            sample (dict): Sample
        """
        img_id = self.ids[idx]
        annotations = self.coco.loadAnns(self.coco.getAnnIds(imgIds=img_id))
        img_path = os.path.join(
            self.root_dir, self.coco.imgs[img_id]['file_name'])
        img = io.read_image(img_path, io.ImageReadMode.RGB)/255
        imgShape = img.shape

        masks = np.zeros(
            (len(self.cats) - 3, img.shape[1], img.shape[2]), dtype=np.float32)

        keypoints = {'CD3': [],
                     'Nkp46': [],
                     'Tryptase': []}

        for annotation in annotations:
            catId = annotation['category_id']
            # print(groundTruth[catId].shape)
            if("keypoint" in annotation):
                # add the keypoint to the dict
                if self.id2cat[catId] in keypoints:
                    keypoints[self.id2cat[catId]].append(
                        annotation['keypoint'])

            elif(len(annotation["segmentation"][0]) > 4) and catId<2:
                mask = self.coco.annToMask(annotation)
                # print(f"CATID = {catId}, {annotation}")
                masks[catId] = np.logical_or(
                    masks[catId], mask)

        CD3 = np.floor(np.array(keypoints["CD3"])).astype(int)
        Nkp46 = np.floor(np.array(keypoints["Nkp46"])).astype(int)
        Tryptase = np.floor(np.array(keypoints["Tryptase"])).astype(int)

        points = [Nkp46, Tryptase, CD3]


        if self.transforms is not None:
            # print("TRANSOFRMING")
            augmented = self.transforms(image=img.numpy().transpose(
                1, 2, 0), mask=masks.transpose(1, 2, 0))
            img, masks = augmented["image"], augmented["mask"]

        return img, masks, points

    def get_cat_id(self, cat_name):
        return self.cat2id[cat_name]

    def get_cat_name(self, cat_id):
        return self.id2cat[cat_id]


def extract_contours(dataset, masks, eps=0.002, kernel_size=11, min_size=500):
    """
    Extract the contours from a mask
    :param masks: a list of masks one for each channel"""
    contours = []

    kernel = cv2.getStructuringElement(
        cv2.MORPH_ELLIPSE, (kernel_size*2+1, kernel_size*2+1))

    kernel2 = cv2.getStructuringElement(
        cv2.MORPH_ELLIPSE, (kernel_size, kernel_size))

    for i, mask in enumerate(masks):

        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel2)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

        mask = cv2.GaussianBlur(mask, (101, 101), 0)
        mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)[1]

        if(mask.sum() == 0):
            continue

        externals, _ = cv2.findContours(
            mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # print(type(output), len(output))
        # if type(output) == tuple and len(output) == 2:
        #     externals, _ = output
        # else:
        #     continue

        name = dataset.id2cat[i]
        color = dataset.name2color[name]
        if len(externals) > 0:
            # print(len(externals))
            contours += list((cv2.approxPolyDP((external).squeeze(1), eps * cv2.arcLength(
                (external).squeeze(1), True), True).squeeze(1), name, color) for external in externals if len(external) > min_size or i != 3)

    return contours
