# Master internship Project

Segmentation of regions and cell detection in lung cancer wsi images.
  


# Setup the project









# Train on costum dataset
```bash
usage: train.py [-h] [--model MODEL] [--batchSize BATCHSIZE] [--epochs EPOCHS]
                [--lr LR] [--logDir LOGDIR]
                [--ckpt_save_top_k CKPT_SAVE_TOP_K] [--gpus GPUS]
                [--seed SEED]
                [--resume_from_checkpoint RESUME_FROM_CHECKPOINT]
                [--rootDir ROOTDIR] [--dataset DATASET] [--annFile ANNFILE]
                [--task TASK] [--numWorkers NUMWORKERS]
                [--prefetchFactor PREFETCHFACTOR] [--dp DP] [--hl HL]
                [--loss LOSS] [--hflip HFLIP] [--vflip VFLIP]
                [--rotate ROTATE] [--scale SCALE] [--shear SHEAR]
                [--blur BLUR] [--brightness BRIGHTNESS] [--contrast CONTRAST]
                [--hue HUE] [--saturation SATURATION] [--gamma GAMMA]
                [--noise NOISE] [--imgSize IMGSIZE]

Train a model

optional arguments:
  -h, --help            show this help message and exit
  --model MODEL         model to train (default: unet)
  --batchSize BATCHSIZE
                        batch size (default: 1)
  --epochs EPOCHS       number of epochs (default: 100)
  --lr LR               learning rate (default: 0.001)
  --logDir LOGDIR       log directory (default: logs)
  --ckpt_save_top_k CKPT_SAVE_TOP_K
                        checkpoint save top k (default: 1)
  --gpus GPUS           number of gpus (default: 1)
  --seed SEED           random seed (default: 42)
  --resume_from_checkpoint RESUME_FROM_CHECKPOINT
                        resume from checkpoint (default: None)
  --rootDir ROOTDIR     root directory (default: /data)
  --dataset DATASET     dataset to train on (default: icy)
  --annFile ANNFILE     annotation file (default: annotations.json)
  --task TASK           task to train (default: segmentation)
  --numWorkers NUMWORKERS
                        number of workers (default: 0)
  --prefetchFactor PREFETCHFACTOR
                        prefetch factor (default: 2)
  --dp DP               deep supervision (default: False)
  --hl HL               task weighting (default: False)
  --loss LOSS           loss function (default: bce)
  --hflip HFLIP         horizontal flip probability (default: 0.5)
  --vflip VFLIP         vertical flip probability (default: 0.5)
  --rotate ROTATE       rotate probability (default: 0.5)
  --scale SCALE         scale probability (default: 0.5)
  --shear SHEAR         shear probability (default: 0.5)
  --blur BLUR           blur probability (default: 0.5)
  --brightness BRIGHTNESS
                        brightness probability (default: 0.5)
  --contrast CONTRAST   contrast probability (default: 0.5)
  --hue HUE             hue probability (default: 0.5)
  --saturation SATURATION
                        saturation probability (default: 0.5)
  --gamma GAMMA         gamma probability (default: 0.5)
  --noise NOISE         noise probability (default: 0.5)
  --imgSize IMGSIZE     image size (default: 256)
```

# Inference

# Ressources

- Icy software:
- GSODA
- Vector Morphological Operator
